#!/bin/bash
# bash is required for pushd and popd (see https://stackoverflow.com/a/5193087/424245)

# Create the target folder, if needed
mkdir -p scripts

## Download gitlab-smalltalk-ci
echo 'Downloading and extracting gitlab-smalltalk-ci'
wget -q -O gitlab-smalltalk-ci.zip https://gitlab.com/SeanDeNigris/gitlab-smalltalk-ci/repository/master/archive.zip
unzip -q -o gitlab-smalltalk-ci.zip

## Copy scripts, but only if missing. Don't overwrite anything!
pushd gitlab-smalltalk-ci-* > /dev/null
cp -n scripts/*  ../scripts 
popd > /dev/null
