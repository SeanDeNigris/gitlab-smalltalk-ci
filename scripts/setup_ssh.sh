# Adapted from https://docs.gitlab.com/ee/ci/ssh_keys/README.html

##
## Install ssh-agent if not already installed, it is required by Docker.
## (change apt-get to yum if you use an RPM-based image)
##
which ssh-agent || ( apt-get update -yqq && apt-get install openssh-client -yqq --no-install-recommends > /dev/null )

##
## Run ssh-agent (inside the build environment)
##
eval $(ssh-agent -s)

##
## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
## We're using tr to fix line endings which makes ed25519 keys work
## without extra base64 encoding.
## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
##
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

##
## Create the SSH directory and give it the right permissions
##
mkdir -p ~/.ssh
chmod 700 ~/.ssh

##
## Optionally, if you will be using any Git commands, set the user name and
## and email.
##
#git config --global user.email "gitlab@domain.com"
#git config --global user.name "User Name"
