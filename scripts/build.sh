# Adapted from Travis glue code (https://github.com/travis-ci/travis-build/blob/master/lib/travis/build/script/smalltalk.rb), also consulting:
#	- Peter Uhnak's script (https://gist.github.com/peteruhnak/dc25bde74711ac3dc985e0a14c805b57#file-gitlab-ci-yml) 
# 	- And this simplified one: https://github.com/hpi-swa/smalltalkCI/issues/262#issuecomment-305463998

# Install smalltalkCI
pushd $HOME > /dev/null
echo 'Downloading and extracting smalltalkCI'
wget -q -O smalltalkCI.zip https://github.com/hpi-swa/smalltalkCI/archive/master.zip
unzip -q -o smalltalkCI.zip
pushd smalltalkCI-* > /dev/null
source env_vars
export PATH="$(pwd)/bin:$PATH"
popd > /dev/null
popd > /dev/null
  
echo "Run SmalltalkCI"
smalltalkci -s "$1"
