The gitlab-smalltalk-ci project brings the magic of smalltalkCI to GitLab by performing three roles:
1. The glue which connects Gitlab and smalltalkCI. As a bonus, it does all the necessary in-container work to setup SSH for private Gitlab projects, so you'd just have to create/enable the deploy key in the Gitlab web UI.
2. A Pharo library to create/modify CI artifacts like configuration files. This let's you write Smalltalk like this:
    ```smalltalk
GitLabCIConfiguration new smalltalkImageTemplateFor: 'Pharo-6.1'
```
    instead of being forced to remember and write: 

    ```yaml
    .pharo61_template: &pharo61
      variables:
        SMALLTALK_IMAGE: "Pharo-6.1"
    ```
3. An example to adapt for your own project, since it eats its own dog food and is handling its own CI.

Also, note that the initial setup is simplified from smalltalkCI because it uses a Docker image preloaded with all the Pharo dependencies. All this has been tested extensively with real projects, but only on Pharo 6/7 and Ubuntu 32 bit. PRs welcome!

# Usage

1. Create a `.gitlab-ci.yml` in your project's root folder. You can use the one from this project [.gitlab-ci.yml](https://gitlab.com/SeanDeNigris/gitlab-smalltalk-ci/blob/master/.gitlab-ci.yml) as inspiration.
2. Ditto for `.smalltalk_ston`. See the smalltalkCI docs [here](https://github.com/seandenigris/smalltalkCI#minimal-smalltalkston-template) and [here](https://github.com/seandenigris/smalltalkCI#setting-up-a-custom-smalltalkston), as well as [the example from this project](https://gitlab.com/SeanDeNigris/gitlab-smalltalk-ci/blob/master/.smalltalk.ston).
3. If you want to customize, simply put a script in `scripts` with the same name as one of our scripts. Any scripts you provide in this way will be used instead of the default ones.
