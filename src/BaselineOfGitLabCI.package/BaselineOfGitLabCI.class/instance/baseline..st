baseline
baseline: spec
	<baseline>
	spec for: #'common' do: [
		spec 
			baseline: #'Mocketry' with: [
				spec repository: 'github://dionisiydk/Mocketry' ];
			package: #GitLabCI with: [ spec requires: #'Mocketry' ] ].
