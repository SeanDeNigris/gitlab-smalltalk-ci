tests
testBuildTemplate

	(GitLabCIConfiguration new buildTemplate) should equal: '.build_template: &build
  stage: build
  script:
    - source scripts/build.sh "$SMALLTALK_IMAGE"
  artifacts:
    when: on_failure
    paths:
    - ./*.fuel # Can''t start with * (see https://gitlab.com/gitlab-org/gitlab-runner/issues/2086)' withUnixLineEndings
	