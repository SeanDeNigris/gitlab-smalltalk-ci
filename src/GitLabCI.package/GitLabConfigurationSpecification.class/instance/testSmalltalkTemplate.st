tests
testSmalltalkTemplate

	(GitLabCIConfiguration new smalltalkImageTemplateFor: 'Pharo-6.1') should equal: '.pharo61_template: &pharo61
  variables:
    SMALLTALK_IMAGE: "Pharo-6.1"' withUnixLineEndings
	