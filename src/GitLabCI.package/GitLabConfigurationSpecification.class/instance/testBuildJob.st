tests
testBuildJob

	(GitLabCIConfiguration new buildJobFor: 'Pharo-6.1') should equal: 'pharo61_build:
  <<: *pharo61
  <<: *build' withUnixLineEndings