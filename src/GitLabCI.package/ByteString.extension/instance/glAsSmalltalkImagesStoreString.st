*GitLabCI
glAsSmalltalkImagesStoreString
	"Copy paste the image list from https://github.com/seandenigris/smalltalkCI/blob/master/run.sh in select_smalltalk() into a Smalltalk string. Then send this message to it"

	| tokens |
	tokens := self findTokens: ' ' , Character lf asString , Character cr asString.
	^ tokens sort asArray storeString