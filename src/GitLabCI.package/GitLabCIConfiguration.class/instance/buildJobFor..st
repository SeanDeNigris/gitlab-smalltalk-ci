accessing
buildJobFor: aString 
	^ '{templateAlias}_build:
  <<: *{templateAlias}
  <<: *build' withUnixLineEndings format: { #templateAlias -> aString glAsYamlToken } asDictionary