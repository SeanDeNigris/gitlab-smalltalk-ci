magritte
gtInspectorContentsIn: composite
	<gtInspectorPresentationOrder: 1>

	^ composite text
			title: 'Contents';
			format: [ self contents ];
			yourself
			