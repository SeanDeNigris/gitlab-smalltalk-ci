private
templateAlias: aliasString for: parametersString with: variables

	| template |
	template := '.{alias}_template: &{alias}
', parametersString.
	^ template withUnixLineEndings format: { #alias -> aliasString } asDictionary, variables asDictionary.