private
smalltalkImageTemplateFor: imageName

	^ self 
		templateAlias: imageName glAsYamlToken
		for: '  variables:
    SMALLTALK_IMAGE: "{imageName}"'
		with: {	#imageName -> imageName }.