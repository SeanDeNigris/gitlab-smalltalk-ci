accessing
contentsOn: aStream
	aStream
		nextPutAll: self header;
		lf; lf;
		nextPutAll: self buildTemplate;
		lf; lf.
		
	self smalltalkImages
		do: [ :image |
			aStream
				nextPutAll: (self smalltalkImageTemplateFor: image);
				lf; lf;
				nextPutAll: (self buildJobFor: image) ] 
		separatedBy: [ aStream lf; lf ]