accessing
contents
	^ String streamContents: [ :s | self contentsOn: s ]