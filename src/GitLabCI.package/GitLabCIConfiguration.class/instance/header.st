accessing
header
	^ 'image: seandenigris/pharo

# Run before *all* jobs, including deploy (https://docs.gitlab.com/ce/ci/yaml/#before_script)
before_script:
  # Install glue scripts
  - wget -q https://gitlab.com/SeanDeNigris/gitlab-smalltalk-ci/raw/master/install_scripts.sh
  - source install_scripts.sh
  - source scripts/setup.sh # Use `source` per https://stackoverflow.com/questions/46030051/gitlab-pipeline-works-in-yml-fails-in-extracted-sh/46041447#46041447

stages:
  - build' withUnixLineEndings